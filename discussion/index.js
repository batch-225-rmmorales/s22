// s22 array manipulation

//push()
//adds and returns num of element

let mors = [1,2,3,4,5];
mors.push(100);
console.log(mors);
mors.push(12,3);

//pop()
//remove last and returns last element

mors.pop();

//unshift()
//adds one or more elements at the beginning of an array
//returns number of array

mors.unshift(1,2,3);


//shift()
// remove element at beginning and returns the removed item

mors.shift();

//splice()
// simultaneously removes element from a specified index number and adds element
// array.splice(startingIndex, deleteCount, elements to be added)

mors.splice(1,1,100,200);
//possible na empty ung i a add

//sort()
//sort by alphanumeric order

//reverse() sort by reverse current order
mors.reverse() vs mors.sort()

//non mutator functions
//indexOf
//return index of first match... if no match was found return -1
// search process is done from first element 
mors.indexOf("abc")

//slice (gives portion of array from starting index and just before ending index)
mors.slice(2,4);

// forEach();
//array.forEach(function(currentValue, index, arr), thisValue)
//https://www.w3schools.com/jsref/jsref_foreach.asp

// forEach();

/*
	- Similar to a for loop that iterates on each array element.
	- For each item in the array, the anonymous function passed in forEach() method will be run.
	- arrayName - plural for good practice
	-parameter - singular
	- Note: it must have function.

	- Syntax:
		arrayName.forEach(function(indivElement){
			statement;
		})
*/

countries.forEach(function(country) {
	console.log(country);
});

//includes() returns true if item is found or false otherwise
array.includes(item)

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");

        console.log(productFound);//returns true

        let productFound2 = products.includes("Headset");

        console.log(productFound);//returns false